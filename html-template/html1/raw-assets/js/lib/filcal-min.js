/*! filcal version.1.0 | 
	(Author) Filjumar Jumamoy 
	(Author LinkedIn) https://www.linkedin.com/in/filjumar-jumamoy-26243182/ 
*/

"use strict";

$(window).on('load',function() {

	var $cal = $('#fil-cal'),
		$date_now = new Date(),
		$date_y = $date_now.getFullYear(),
		$date_m = $date_now.getMonth()+1,
		$date_d = $date_now.getDate(),
		$num_days_now = 0,
		$new_date = new Date(),
		$new_y = $new_date.getFullYear(),
		$new_m = $new_date.getMonth()+1,
		$new_d = $new_date.getDate(),
		$days = ['Sun','Mon','Tue','Wed','Thur','Fri','Sat'],
		$months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'];

		if($new_m < 10) {
			$new_m = "0"+$new_m;
		}

		var $declared_lang;
		try {
			$use_lang;
			$declared_lang = true;
		} catch(e) {
			$declared_lang = false;
		}
		  
		if ($declared_lang) {
			if($use_lang==="ja" || $use_lang==="zh") {
				$days = ['日','月','火','水','木','金','土'];
				$months = ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'];
			}else{
				$days = ['Sun','Mon','Tue','Wed','Thur','Fri','Sat'];
				$months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'];
			}
		} else {
			$days = ['Sun','Mon','Tue','Wed','Thur','Fri','Sat'];
			$months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'];
		}
		
		var $declared_posts;
		try {
			$post_array;
			$declared_posts = true;
		} catch(e) {
			$declared_posts = false;
		}


	function loadCalendar($date_m, $date_y, $date_d) {
		
		$num_days_now = new Date($date_y, $date_m, 0).getDate();

		var $i = 1,
			$x = 0,
			$y = 1,
			$new_i = 0,
			$i_counter = -1,
			$counter = 0,
			$tbl_td_count = 42,
			$tbl_contents = "",
			$yrs_strt = $date_y - 15,
			$yrs_end = $date_y + 15;

		$cal.html("<table cellspacing='0' cellpadding='5'><tbody></tbody></table>");
		// days
		$tbl_contents += "<thead>";
		$tbl_contents += "<tr id='option-area'>"+
							"<th><a href='javascript:;' class='option_btn' data-option='prev'> <span> < </span> </a></th>"+
							"<th><a href='javascript:;' id='select_date'>"+$date_y+" "+$months[$date_m-1]+"</a></th>"+
							"<th><a href='javascript:;' class='option_btn' data-option='next'> <span> > </span> </a></th>"+
						"</tr>";
		$tbl_contents += "<tr><th id='year_lists'><ul>";

		for($yrs_strt; $yrs_strt<=$yrs_end; $yrs_strt++) {
			$tbl_contents += "<li><a href='javascript:;' id='drop_yr' data-drop='"+$yrs_strt+"'>"+$yrs_strt+"</a></li>";
		}
					
		$tbl_contents += "</th></tr>";
		for( $x; $x<$days.length; $x++ ) {

			$tbl_contents += "<th><span>"+$days[$x]+"</span></th>";
			
		}
		
		$tbl_contents += "</thead>";

		$tbl_contents += "<tr>";
		
		for( $i; $i<=$tbl_td_count; $i++ ) {

			$new_i = parseInt($i-1);
			
			if( $new_i <= 0 ) {
				$new_i = new Date($date_y, parseInt($date_m-$i_counter), 0).getDate();
			}else if( $new_i > $num_days_now ) {
				$counter++;
				$new_i = $counter;
			}

			$i_counter++;
			$tbl_contents += "<td data-day='"+$i_counter+"' class='col_num"+$i_counter+"'>";
			
			if( $i%7 == 0 ) {
				$tbl_contents += "</td></tr>";
			}
			
		}

		$cal.find('table tbody').append($tbl_contents);
		
		var $count_con = 1;
		var $array_count = 0;

		var $starting_point = new Date($date_y+"/"+$date_m+"/01").getDay();
		if($date_m<10) {
			$date_m = "0"+$date_m;
		}
		if($date_d<10) {
			$date_d = "0"+$date_d;
		}
		var $date_now_comp = $date_y+"/"+$date_m+"/"+$date_d;
	
		$('.col_num'+$starting_point)
			.attr("data-date",$date_y+"/"+$date_m+"/01")
			.html("<span class='day_count'>1</span>")
			.attr("data-start","start");

		var $cur_date = new Date(),
			$cur_yr = $cur_date.getFullYear(),
			$cur_m = $cur_date.getMonth()+1,
			$cur_d = $cur_date.getDate();

		if($cur_m<10) {
			$cur_m = "0"+$cur_m;
		}
		if($cur_d<10) {
			$cur_d = "0"+$cur_d;
		}	

		if($cur_yr+"/"+$cur_m+"/"+$cur_d == $date_now_comp) {
			$("[data-date='"+$date_now_comp+"']").addClass('active');
		}

		var $data_day_start = $("[data-start]").data('day');
		
		for($y = $data_day_start + 1; $y<=$num_days_now+$data_day_start-1 ; $y++) {
			$count_con++;
			if($count_con<10) {
				$count_con = "0"+$count_con;
			}
			$date_now_comp = $date_y+"/"+$date_m+"/"+$count_con;
			
			$('[data-day='+$y+']')
				.attr("data-date",$date_now_comp)
				.html("<span class='day_count'>"+$count_con+"</span>");
			
			if(new Date($date_y+"/"+$date_m+"/01").getDay() == 0) {
				$('[data-day=0]').find('.day_count').addClass('is-weekend is-saturday');
			}	
			if(new Date($date_y+"/"+$date_m+"/01").getDay() == 6) {
				$('[data-day=6]').find('.day_count').addClass('is-weekend is-sunday');
			}
			if( $date_y+"/"+$date_m+"/01" == $new_y+"/"+$new_m+"/"+$new_d ) {
				$('[data-date="'+ $date_y+"/"+$date_m+'/01]').addClass('active');
			}
	
			if(new Date($date_now_comp).getDay() == 0) {
				$('[data-day='+$y+']').find('.day_count').addClass('is-weekend is-saturday');
			}	
			if(new Date($date_now_comp).getDay() == 6) {
				$('[data-day='+$y+']').find('.day_count').addClass('is-weekend is-sunday');
			}
			if( $date_y+"/"+$date_m+"/"+$date_d == $new_y+"/"+$new_m+"/"+$new_d ) {
				$('[data-date="'+ $date_y+"/"+$date_m+"/"+$date_d +'"]').addClass('active');
			}
		}
		if($declared_posts) {
			for($array_count; $array_count < $post_array.length; $array_count++) {
				$('[data-date="'+$post_array[$array_count].date+'"]')
					.addClass('c-post')
					.append("<span class='post-title'>"+$post_array[$array_count].title+"</span>"+
							"<div class='hidden_infos'>"+
							"<h3 class='c_pop-tit'>"+$post_array[$array_count].title+"</h3>"+
							"<div class='c_pop-infos'>"+
							"<span class='c_pop-date'>Event Date: "+$post_array[$array_count].date+" "+$post_array[$array_count].time_start+" - "+$post_array[$array_count].time_end+"</span>"+
							"<a href='javascript:;' class='to_post' data-url='"+$post_array[$array_count].url+"'>TO POST</a></div>"+
							"</div>"
						);
			}	
		}
	}
	
	loadCalendar($date_m, $date_y, $date_d);

	// click event
	var $hidden_infos = "";
	
	$(document).on('click','.c-post',function(e){
		e.preventDefault();
		var $this_parent = $(this);
		$('.c-post').find('.c-pop').remove();
		
		$('#test').remove();
		
		$cal.append("<div id='test'></div>");

		if($(this).children('.c-pop').length <= 0) {

			$hidden_infos = "<div class='c-pop'>"+
							"<div class='c_pop-body'>"+
							"<a href='javascript:;' id='c_pop-close'></a>"+
							"</div>"+
							"</div>";

			if( $(window).innerWidth() <= 1000 )  {
				$('#test').append($hidden_infos);
				$(this).find('.hidden_infos').each(function(){
					$('#test').find('.c_pop-body').append($(this).html());
				});
				
			}else{
				$cal.find('#test').remove();
				$(this).append($hidden_infos);
				$(this).find('.hidden_infos').each(function(){
					$this_parent.find('.c_pop-body').append($(this).html());
				});
			}
			
			
		}
		
		return false;
	})

	// to post
	$(document).on('click','.to_post',function(){
		window.open($(this).data("url"),"_blank");
	});
	// popup close
	$(document).on('click','#c_pop-close',function(e){
		e.preventDefault();
		$('.c-pop').remove();
		$('#test').remove();
		return false;
	});

	// next month
	var $option_counter = 0;
	$(document).on('click','.option_btn',function(e){
		if($(this).data('option') === "next") {
			$option_counter++;
			$date_m++;
	
			if($date_m>12) {
				$date_y++;
				$date_m = 1;
				$option_counter = 0;
			}
		}else{
			$option_counter--;
			$date_m--;

			if($date_m<1) {
				$date_y--;
				$date_m = 12;
				$option_counter = 0;
			}
		}
		

		loadCalendar($date_m, $date_y, $date_d);
	});

	$(document).on('click','#select_date',function(e){
		e.preventDefault();
		$('#year_lists').toggleClass("active");
		return false;
	});

	$(document).on('click','#drop_yr',function(e){
		e.preventDefault();
		$date_y = $(this).data('drop');
		loadCalendar($date_m, $date_y, $date_d);

		return false;
	});


});
