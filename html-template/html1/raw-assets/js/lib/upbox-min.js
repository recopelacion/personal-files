"use strict";

(function($){
	
	var $upbox = $('.upbox'),
		$upbox_pop = $('.upbox-pop'),
		$upbox_opt_parent = $('.upbox-opt'),
		$upbox_body = $('.upbox-body'),
		$upbox_np = $('.upbox-np'),
		$upbox_imgIndex = "",
		$upbox_bodyX = "",
		$upbox_bodyY = "",
		$upbox_img = "",
		$upbox_posX = "",
		$upbox_posY = "",
		$upbox_opt = "";

	// var $ul = '<ul>';
	// 	$upbox.each(function(index){
	// 		$ul += '<li><div class="upbox-img">'+
	// 		'<img src="'+$(this).find('img').attr('src')+'" alt="" class="is-wide">'+
	// 		'</div></li>';
	// 	});
	// 	$ul += '</ul>';	
	// $upbox_body.append($ul);

	$upbox.on('click',function(e){
		$upbox_body.find('ul').css({
			opacity: 0,
			transition: 'linear .3s'
		});
		$upbox_img = $(this).find('img').attr('src');
		$upbox_posX = $(this).position().left;
		$upbox_posY = $(this).position().top;

		$(this).find('img').addClass('is-zoom');

		$upbox_bodyX = $upbox_pop.find($upbox_body).position().left;
		$upbox_bodyY = $upbox_pop.find($upbox_body).position().top;

		setTimeout(function(){
			$upbox_pop.css({
				opacity: 1,
				zIndex: 99,
				transition: 'ease-in .2s'
			});
		},250);
		setTimeout(function(){
			$upbox_body.find('ul').css({
				opacity: 1,
				transition: 'linear .3s'
			});
		},500);
		setTimeout(function(){
			$upbox.find('img').removeClass('is-zoom');
		},1000);

		$upbox_body.find('[src="'+$upbox_img+'"]').parents("ul").prepend($upbox_body.find('[src="'+$upbox_img+'"]').parents("li"));
	});

	$upbox_opt_parent.find('li').children('a').on('click',function(e){
		if($(this).data('opt') == 'close') {
			$upbox_pop.css({
				opacity: 0,
				zIndex: -1,
				transition: 'linear .3s'
			});
		}
	});

	var $counter = 0;

	$upbox_np.find('.n_p').on('click',function(e){
		e.preventDefault();
		if($(this).data('opt')=="next") {
			// $upbox_body.find('ul').append($upbox_body.find('li:eq(0)'));
			$counter++;

			if($counter > $upbox_body.find('ul li').length - 1 ) {
				$counter = 0;
			}
			$upbox_body.find('ul').prepend($upbox_body.find('li:eq('+$counter+')'));
		}else{
			$counter--;

			if($counter<0) {
				$counter = $upbox_body.find('ul li').length - 1;
			}
			$upbox_body.find('ul').prepend($upbox_body.find('li:eq('+$counter+')'));
		}
		return false;
	});

}) (jQuery);