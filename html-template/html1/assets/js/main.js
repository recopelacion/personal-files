(function($){
	// navigation menu
	var $nav = $('.main-navigation'),
		$toggle_nav = $('#toggle-nav');

		$toggle_nav.click(function(e){
			e.preventDefault();
			var $this = $(this);

			if($this.hasClass('toggle-open')) {
				$this.removeClass('toggle-open');
				$nav.css({
					left: '-100%',
					transition: 'ease-out .2s'
				});
			}else{
				$this.addClass('toggle-open');
				$nav.css({
					left: '0',
					transition: 'ease-in .2s'
				});
			}
			
			
			return false;
		});

}) (jQuery);

$(window).on('resize', function(){
	var $nav = $('.main-navigation'),
		$toggle_nav = $('#toggle-nav');

	if($(this).innerWidth() > 768) {
		$toggle_nav.removeClass('toggle-open');
		$nav.css({
			left: '0',
			transition: 'ease-out .2s'
		});
	}else{
		$nav.css({
			left: '-100%',
			transition: 'ease-out .2s'
		});
	}

});