$(document).ready(function () {

        var alphabet = "abcdefghijklmnopqrstuvwxyz";
        var alphabet_lenght = alphabet.length;
        var abc = 0;
        var ans = "kaguran";
        var ansLength = ans.length;
        var ansLoop = 0;
        var wrongCounter = 0;
        var checkCounter = 0;

        $('.hangmanWrapper').find('span').each(function(){
            $(this).addClass('ans' + parseInt($(this).index() + 1) );
        }); 

        for( abc; abc < alphabet_lenght; abc++ ){
            $('.alphabetBtn').append( '<button class="letBtn">'+ alphabet[abc] +'</button>' );

            $('.letBtn').eq(abc).on('click', function(){

                var lett = $(this).text();
                var dataLet = $(this).attr('data-alphabet', lett);
                dataLet.css({
                    cursor: 'auto'
                }).attr('disabled', 'disabled')

                if(ans.includes(lett)) {
                    $("[data-let="+ lett +"]").each(function(index){
                        checkCounter++
                        $(this).text(lett);
                    })
                    dataLet.css({
                        background: 'green'
                    })
                }else{
                    wrongCounter++
                    $('.ans'+wrongCounter).css({
                        opacity: 1,
                        transition: "linear .3s"
                    })
                    dataLet.css({
                        background: 'red',
                        color: 'white'
                    })
                }

                if( checkCounter == ansLength ){
                    alert('You win')
                }

                if( wrongCounter == ansLength - 1 ){
                    alert('You Lose')
                }


            });
        }

        for( ansLoop; ansLoop < ansLength; ansLoop++ ){
            $('.hangmanAns').append('<span class="letAns">'+ans[ansLoop]+'</span>');
        }

        $('.hangmanAns').children('span').each(function(){
            $(this).attr('data-let', $(this).text());
        });
        
        $('.letAns').text('?');

});