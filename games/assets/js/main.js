(function($){
	// navigation menu
	var $nav = $('.main-navigation'),
		$toggle_nav = $('#toggle-nav');

		$toggle_nav.click(function(e){
			e.preventDefault();
			var $this = $(this);

			if($this.hasClass('toggle-open')) {
				$this.removeClass('toggle-open');
				$nav.css({
					left: '-100%',
					transition: 'ease-out .2s'
				});
			}else{
				$this.addClass('toggle-open');
				$nav.css({
					left: '0',
					transition: 'ease-in .2s'
				});
			}
			
			
			return false;
		});

}) (jQuery);

$(window).on('resize', function(){
	var $nav = $('.main-navigation'),
		$toggle_nav = $('#toggle-nav');

	if($(this).innerWidth() > 768) {
		$toggle_nav.removeClass('toggle-open');
		$nav.css({
			left: '0',
			transition: 'ease-out .2s'
		});
	}else{
		$nav.css({
			left: '-100%',
			transition: 'ease-out .2s'
		});
	}

});


$(document).ready(function(){
	
	var dataAns = $('#picWord').attr('data-answer');
	var dataAnswer = $('#picWord').attr('data-answer').length;
	var i = 0;
	var r = 0;
	var result = '';
	var characters = [
	  "taxi",
	  "driver",
	  "red",
	  "fruits",
	  "sugar",
	  "balloons"
	];
	var charactersLength = characters.length;
  
	for( i; i < dataAnswer; i++ ){
	  $('.boxAns').append('<div class="letter"></div>');
	}
  
	var randChar = Math.floor(Math.random() * charactersLength);
	var addChar = characters[randChar];
	var newChars = dataAns + addChar;
	var newCharsSplit = newChars.split("");
	var newLength = newCharsSplit.length;
  
  
	for(var r = newLength - 1; r > 0; r--) {
		var j = Math.floor(Math.random() * (r + 1));
		var tmp = newCharsSplit[r];
		newCharsSplit[r] = newCharsSplit[j];
		newCharsSplit[j] = tmp;
	}
  
	var newData = newCharsSplit.join("");
  
	for( var x = 0; x < newData.length; x++ ){
	  $('.boxLetters').append( "<button class='lettersBtn'> " + newData[x] + " </button>" )
	}
  
	var clickedL = "",
		myAns = [],
		myAnsVal = "";
	$('.lettersBtn').on('click', function(){
  
	  clickedL = $(this).attr('disabled','disabled').text();
	  myAns.push(clickedL)
	  $('.boxAns').children('.letter').each(function(index){
		$(this).text(myAns[index]);
	  })
  
	  if( $('.boxAns').children('.letter').length == myAns.length) {
  
		for(var z = 0; z < myAns.length; z++) {
		  myAnsVal+=myAns[z]
		}
  
		myAnsVal = myAnsVal.split(" ")
		myAnsVal = myAnsVal.join("");
  
		if(myAnsVal === dataAns) {
		  $('.letter').addClass('success')
		}else{
		  $('.letter').addClass('error')
		  return false
		}
	   
	  }
  
  
	});
	
  });